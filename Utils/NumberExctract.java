package Utils;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberExctract {

	private ArrayList<Integer> ints = new ArrayList<Integer>();
		
	    public NumberExctract(String str){
	        Pattern p = Pattern.compile("\\d+");
	        Matcher m = p.matcher(str);
	        while(m.find()) {
	            ints.add(Integer.parseInt(m.group()));
	        }
	    }
	    
	    public ArrayList<Integer> getInts(){
	    	return ints;
	    }
	    
	}
	
