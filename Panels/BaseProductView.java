package Panels;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Products.BaseProduct;


public class BaseProductView extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JTable table;
	private JButton back;
    
    DefaultTableModel defaultTableModel = new DefaultTableModel();
    String data;
    ArrayList<BaseProduct> products;
    
    public BaseProductView(ArrayList<BaseProduct> products) {
    	
    	this.products = products;
    	frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.getContentPane().setLayout(new GridLayout(1,1));
	    frame.setVisible(true);
	    setLayout(new GridLayout(2,1));
	    back = new JButton("Back");
	    back.setBounds(0, 10, 0, 22);
	    back.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frame.setVisible(false);
				
			}
		});
	    
		Object columns[] = {"Id","Nume","Pret"};
        defaultTableModel.setColumnIdentifiers(columns);
		
		table = new JTable();
		
		
		JScrollPane scrollPane = new JScrollPane();
		table.setModel(defaultTableModel);
		scrollPane.setViewportView(table);
		
		add(scrollPane);
        add(back);
        frame.add(this);
        loadData();
    
        
    }
	
	
	 public void loadData() {
	        defaultTableModel.getDataVector().removeAllElements();
	        defaultTableModel.fireTableDataChanged();
	        	
	            Object columns[] = {"Id","Nume","Pret"};
	            defaultTableModel.setColumnIdentifiers(columns);
	            Object columnData[] = new Object[3];
	            for(BaseProduct p : products) {
	            	System.out.println("aici");
	            System.out.println(p.getName() +" "+ p.getPrice()+ " " + p.getId());
	              columnData[0] = p.getId();
	              columnData[1] = p.getName();
	              columnData[2] = p.getPrice();
	              defaultTableModel.addRow(columnData);

	            }
	       
	    }
	 
	 
	 
	

}
