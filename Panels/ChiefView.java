
	
	package Panels;

	import java.awt.GridLayout;
	import java.awt.event.MouseAdapter;
	import java.awt.event.MouseEvent;
	import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.security.auth.x500.X500Principal;
import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JPanel;
	import javax.swing.JScrollPane;
	import javax.swing.JTable;
	import javax.swing.table.DefaultTableModel;

import Products.CompositeProduct;
import Products.MenuItem;
import Restaurant.Restaurant;


	public class ChiefView extends JPanel{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JFrame frame;
		private JTable table;
		private JButton back;
	    
	    DefaultTableModel defaultTableModel = new DefaultTableModel();
	    String data;
	    List<CompositeProduct> cproducts;
	    Restaurant restaurant;
	    public ChiefView(Restaurant restaurant) {
	    	
	    	this.restaurant = restaurant;
	    	//this.cproducts = cproducts;
	    	frame = new JFrame();
			frame.setBounds(100, 100, 450, 300);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//			frame.getContentPane().setLayout(new GridLayout(1,1));
		    frame.setVisible(true);
		    setLayout(new GridLayout(2,1));
		    back = new JButton("Back");
		    back.setBounds(0, 10, 0, 22);
		    back.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					frame.setVisible(false);
					
				}
			});
		    
			Object columns[] = {"Id","Table","Pret"};
	        defaultTableModel.setColumnIdentifiers(columns);
			
			table = new JTable();
			System.out.println("aici");
			
			JScrollPane scrollPane = new JScrollPane();
			table.setModel(defaultTableModel);
			scrollPane.setViewportView(table);
			
			add(scrollPane);
	        add(back);
	        frame.add(this);
	        loadData();
	    
	        
	    }
		
		
		 public void loadData() {
		        defaultTableModel.getDataVector().removeAllElements();
		        defaultTableModel.fireTableDataChanged();
		        	
		            Object columns[] = {"Id","Table","Pret"};
		            defaultTableModel.setColumnIdentifiers(columns);
		            Object columnData[] = new Object[3];
		            for(HashSet<MenuItem> o : restaurant.getData().values()) {
		            	Iterator<MenuItem> i = o.iterator();
		            	while(i.hasNext()) {
		            		CompositeProduct x = (CompositeProduct) i.next();
		            		columnData[0] = x.getId();
		  			        columnData[1] = x.getName();
		  			        columnData[2] = x.computePrice();
		                    defaultTableModel.addRow(columnData);
		            	}
 	        			
		            }
		          
		       
		    }
		 
		 
		 
		

	}



