package Panels;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Products.CompositeProduct;
import Products.MenuItem;
import Products.Order;
import Restaurant.Restaurant;
import Utils.AutoIncrement;
import Utils.NumberExctract;


	
public class WaiterView extends JPanel {
		
		

		private static final long serialVersionUID = 1L;
		private JButton createOrder = new JButton("Create New Order");
		private JButton computePrice = new JButton("Compute Price");
		private JButton generateBill = new JButton("Chef View");
		private JButton back = new JButton("Back");
		private JLabel tableLbl = new JLabel("Table ");
		private JTextField tableTxt = new JTextField("");
		private JLabel tableMenu = new JLabel("Menu item");
		private JTextField menuTxt = new JTextField("");
		private JFrame frame = new JFrame("Administrator Operations");
		private List<CompositeProduct> cproducts ;
		private AutoIncrement auto = new AutoIncrement();
		private Restaurant restaurant = new Restaurant();
		public  WaiterView(List<CompositeProduct> products) {
			
			this.cproducts = products;
			frame.setSize(560, 300);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);

			setLayout(new GridLayout(3,2));
			add(createOrder);
			add(computePrice);
			add(generateBill);
			add(new JLabel(""));
			add(tableLbl);
			add(tableTxt);
			add(tableMenu);
			add(menuTxt);
			add(back);
			
			backListener();
			createOrder();
			generateBill();
			computePrice();
			setVisible(true);
			
			frame.add(this);
		}
		
	
		public void backListener() {
			back.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					frame.setVisible(false);
					
				}
			});
		}
		
		public void createOrder() {
			createOrder.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					int table = Integer.parseInt(tableTxt.getText());
					Order o = new Order(auto.id++,new Date(System.currentTimeMillis()),table);
					restaurant.addOrder(o.getOrderId(),o.getDate(),o.getTable());
					String str = menuTxt.getText();
					NumberExctract pattern = new NumberExctract(str);
					System.out.println(pattern.getInts());
					for(Integer i : pattern.getInts()) {
						//System.out.println();
						//name += cproducts.get(i-1).getName();
						restaurant.addMeniu(o, cproducts.get(i-1));
						
					}
					//restaurant.addMeniu(o, cproducts);
					
					tableTxt.setText("");
					menuTxt.setText("");
					
				}
			});
		}
		public void generateBill() {
			generateBill.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					new ChiefView(restaurant);
				}
			});
		}
		
		public void computePrice() {
			computePrice.addMouseListener(new MouseAdapter() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					int table = Integer.parseInt(tableTxt.getText());
					PrintWriter wr;
					try {
						Order o = restaurant.getOrder(table);
						if(o != null) {
							HashSet<MenuItem> menu = restaurant.getMenu(o);
							Iterator<MenuItem> i = menu.iterator();
							double sum = 0;
							while(i.hasNext()) {
								sum += ((CompositeProduct)i.next()).computePrice();
							}
							wr = new PrintWriter("bill.txt","UTF-8");
							wr.println(o.getOrderId() +" " + o.getTable() +" " + sum );
							wr.close();
							System.out.println("success!");
						}
						
					} catch (Exception e2) {
						// TODO: handle exception
					}
				}
			});
		}
		

}
