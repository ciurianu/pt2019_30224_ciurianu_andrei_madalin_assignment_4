package Panels;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Products.BaseProduct;
import Products.CompositeProduct;
import Utils.AutoIncrement;
import Utils.NumberExctract;


	
public class AdminView extends JPanel {
		
		

		private static final long serialVersionUID = 1L;
		private JButton createBase = new JButton("Create Base Product");
		private JButton createMenu = new JButton("Create MenuItem");
		private JButton showMenu = new JButton("Show MenuItem");
		private JButton showBase = new JButton("Show Base Product");
		private JButton edit = new JButton("Edit Menu");
		private JButton delete = new JButton("Delete Menu");
		private JButton back = new JButton("Back");
		private JTextField baseProduct = new JTextField("");
		private JLabel baseP = new JLabel("BaseProductId: ");
		private JTextField menuId = new JTextField("");
		private JLabel menu = new JLabel("MenuId: ");
		
		private JFrame frame = new JFrame("Administrator Operations");
		private List<CompositeProduct> cproducts = new ArrayList<CompositeProduct>();
		private ArrayList<BaseProduct> products;
		private AutoIncrement auto = new AutoIncrement();
		public  AdminView(ArrayList<BaseProduct> products) {
			
			this.products = products;
			frame.setSize(560, 300);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);

			setLayout(new GridLayout(3,2));
			add(baseP);
			add(baseProduct);
			add(menu);
			add(menuId);
			//add(new JLabel());
			//add(new JLabel());
			add(showBase);
			add(createBase);
			add(createMenu);
			add(showMenu);
			add(edit);
			add(delete);
			add(back);
			
			backListener();
			showBaseProduct();
			createMenu();
			showMenu();
			editMenu();
			setVisible(true);
			
			frame.add(this);
		}
		
		public void createListener() {
			createBase.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					//frame.setVisible(false);
					
					
				}
			});
		}
		
		public void showMenu() {
			showMenu.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					new MenuItemView(cproducts);
					
				}
			});
		}
		public void editMenu() {
			edit.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					int menuid = Integer.parseInt(menuId.getText());
					String str = baseProduct.getText();
					NumberExctract pattern = new NumberExctract(str);
					String name = "";
					System.out.println(pattern.getInts());
					for(Integer i : pattern.getInts()) {
						name += products.get(i-1).getName();
						name +=" ,";
					}
					System.out.println(name);
					CompositeProduct compProduct = new CompositeProduct(menuid,name);
					for(Integer i : pattern.getInts()) {
						compProduct.add(products.get(i-1));
					}
					//cproducts.add(menuid, compProduct);
					for(CompositeProduct cp : cproducts) {
						if(cp.getId() == menuid) {
							cproducts.remove(cp);
						}
					}
					//cproducts.remove(menuid-1);
					cproducts.add(compProduct);
					baseProduct.setText("");
					menuId.setText("");
					
				}
			});
		}
		public void createMenu() {
			createMenu.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					String str = baseProduct.getText();
					NumberExctract pattern = new NumberExctract(str);
					String name = "";
					System.out.println(pattern.getInts());
					for(Integer i : pattern.getInts()) {
						//System.out.println();
						name += products.get(i-1).getName();
						name +=" ,";
					}
					System.out.println(name);
					CompositeProduct compProduct = new CompositeProduct(auto.id++,name);
					for(Integer i : pattern.getInts()) {
						compProduct.add(products.get(i-1));
					}
					cproducts.add(compProduct);
					baseProduct.setText("");
					
				}
			});
		}
		public void showBaseProduct() {
			showBase.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					new BaseProductView(products);
				}
			});
		}
		public void Delete(final ActionListener actionListener) {
			delete.addActionListener(actionListener);
		}
		public void backListener() {
			back.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					frame.setVisible(false);
					
				}
			});
		}
		
		public List<CompositeProduct> getComposite(){
			
			return cproducts;
		}
		

}
