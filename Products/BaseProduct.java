package Products;

import java.util.ArrayList;
import java.util.List;

public class BaseProduct extends MenuItem {

	private int id=0;
	private double price;
	private String name;


	
	public  BaseProduct() {
			this.setPrice(0);
			this.setName(null);
			this.setId(0);
	}
	
	public BaseProduct(String name, double price,int id) {
		this.setName(name);
		this.setPrice(price);
		this.setId(id);
	
	}
	
	@Override
	public double computePrice() {
		
		return this.getPrice();
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
