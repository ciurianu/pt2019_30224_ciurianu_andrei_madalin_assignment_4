package Products;

import java.util.Date;

public class Order {
	
	private int OrderId;
	private Date date;
	private int table;
	
	public Order(int OrderId,Date date, int table) {
		this.OrderId = OrderId;
		this.date = date;
		this.table = table;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + OrderId;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + table;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (OrderId != other.OrderId)
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (table != other.table)
			return false;
		return true;
	}

	public int getOrderId() {
		return OrderId;
	}

	public void setOrderId(int orderId) {
		OrderId = orderId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}
	
	
	

}
