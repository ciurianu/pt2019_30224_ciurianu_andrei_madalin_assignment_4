package Products;


import java.util.ArrayList;
import java.util.List;


public class CompositeProduct extends MenuItem{
	
	private double price;
	private String name;
	private int id;
	private List<MenuItem> products = new ArrayList<MenuItem>();
	
	public CompositeProduct(int id,String name) {
		this.name = name;
		this.price = 0;
		this.setId(id);
	}
	
	

	@Override
	public double computePrice() {
		double sum = 0;
		for(MenuItem item : products) {
			sum += ((BaseProduct)item).computePrice();
		}
		return sum;
	}


	public void add(MenuItem item) {
		products.add(item);
		
	}
	
	public void remove(MenuItem item) {
		products.remove(item);
	}
	
	public double getPrice() {
		return price;
	}



	public void setPrice(double price) {
		this.price = price;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public List<MenuItem> getProducts() {
		return products;
	}



	public void setProducts(List<MenuItem> products) {
		this.products = products;
	}
	
	@Override
	public String toString() {
	
		System.out.println("Meniu : " + this.name);
		System.out.println("Continut Meniu : ");
		System.out.println("------------------------------");
		for(MenuItem item : getProducts()) {
			System.out.println("Nume : " + ((BaseProduct)item).getName() + "  " + "Pret : " + ((BaseProduct)item).getPrice());
		}
		System.out.println("\nPret total meniu : " + this.computePrice());
		
		return null;
	}

	@Override
	public int compareTo(CompositeProduct m) {
		
		return m.id - this.id;
		//return m.deg - this.deg;
	}
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}


}

	
