package MainController;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.sun.javafx.geom.AreaOp.AddOp;

import MainPanel.MainPanel;
import Panels.AdminView;
import Panels.WaiterView;
import Products.BaseProduct;
import Products.CompositeProduct;




public class MainController {
	
	MainPanel mainP;
	AdminView Aview;
	ArrayList<BaseProduct> products;
	List<CompositeProduct> cproducts;
	public MainController(ArrayList<BaseProduct> products) {
		mainP = new MainPanel();
		this.products = products;
		initializeActionListeners();
	
	}
	
	
	
	private void initializeActionListeners() {
			
			
		AdministratorOp();
		WaiterOp();
		ChefView();
		
		}
	
	private void AdministratorOp() {
		mainP.AdministratorOp(e->{
			
			this.cproducts = new AdminView(products).getComposite();
		});
	}
	private void WaiterOp() {
		mainP.WaiterOp(e->{
			new WaiterView(cproducts);
		});
	}
	private void ChefView() {
		mainP.ChefView(e->{
			//new EditCustomer();
		});
	}
//	private void initializeDeleteCustomers() {
//		mainP.deleteCustomer(e->{
//			new DeleteCustomer();
//		});
//	}
//	private void initializeViewWarehouse() {
//		mainP.viewWarehouse(e->{
//			new WarehouseView();
//		});
//	}
//	private void initializeViewOrders() {
//		mainP.viewOrders(e->{
//			new OrdersView();
//		});
//	}
//	private void initializeAddProduct() {
//		mainP.addProduct(e->{
//			new AddProduct();
//		});
//	}
//	private void initializeRemoveProduct() {
//		mainP.removeProduct(e->{
//			new RemoveProduct();
//		});
//	}
//	private void initializeUpdateStock() {
//		mainP.updateStock(e->{
//			new UpdateStock();
//		});
//	}
//	private void initializeAddOrder() {
//		mainP.addOrder(e->{
//			new ViewCustomerOrder();
//		});
//	}

}
