package MainPanel;
	
	//package MainController;


	import java.awt.GridLayout;
	import java.awt.event.ActionListener;

	import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JPanel;

	//import groups.ShopCustomers;

	public class MainPanel extends JPanel {
		
		

		private static final long serialVersionUID = 1L;
		private JButton Administrator = new JButton("Administrator Operations");
		private JButton Waiter = new JButton("Waiter Operations");
		private JButton Chef = new JButton("View Chef");
		
		private JFrame frame = new JFrame("Restaurant Processing");


		public  MainPanel() {
			
			frame.setSize(560, 300);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);

			setLayout(new GridLayout(1,3));

			add(Administrator);
			add(Waiter);
			//add(Chef);
			
			setVisible(true);
			
			frame.add(this);
		}
		
		public void AdministratorOp(final ActionListener actionListener) {
			Administrator.addActionListener(actionListener);
		}
		public void WaiterOp(final ActionListener actionListener) {
			Waiter.addActionListener(actionListener);
		}
		public void ChefView(final ActionListener actionListener) {
			Chef.addActionListener(actionListener);
		}
		

	}


