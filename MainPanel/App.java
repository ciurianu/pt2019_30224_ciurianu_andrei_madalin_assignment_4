package MainPanel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import MainController.MainController;
import Products.BaseProduct;
import Utils.AutoIncrement;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	
    	AutoIncrement auto = new AutoIncrement();
    	BaseProduct bproduct1 = new BaseProduct("pizza diavola",22,auto.id++);
    	BaseProduct bproduct2 = new BaseProduct("cola",2.55,auto.id++);
    	BaseProduct bproduct3 = new BaseProduct("prajitura",4.99,auto.id++);
    	BaseProduct bproduct4 = new BaseProduct("hot dog",3.5,auto.id++);
    	BaseProduct bproduct5 = new BaseProduct("salata",5.5,auto.id++);
    	BaseProduct bproduct6 = new BaseProduct("pui",13.2,auto.id++);
    	ArrayList<BaseProduct> products = new ArrayList<BaseProduct>();
    	products.add(bproduct1);
    	products.add(bproduct2);
    	products.add(bproduct3);
    	products.add(bproduct4);
    	products.add(bproduct5);
    	products.add(bproduct6);


    	new MainController(products);
    	
    }
}
