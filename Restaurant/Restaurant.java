package Restaurant;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.swing.JOptionPane;

import Products.CompositeProduct;
import Products.MenuItem;
import Products.Order;





public class Restaurant {
	
	private int id = 0;
	private HashMap<Order, HashSet<MenuItem>> database;
	
	public Restaurant() {
		
		database = new HashMap<Order, HashSet<MenuItem>>();
		
	}
	
	public void addOrder(int id, Date date, int table) {
		
		assert ( table >= 0 && id >= 0) : "Cannot add with null table/id and/or negative !";
		int preSize = database.size();
		if (isWellFormed()) {
			Order o = new Order(id, date,table);
			if (database.containsKey(o))
				infoBox("Order already exists!", "Add order error.");
			else {
				database.put(o, new HashSet<MenuItem>());
				int postSize=database.size();
				assert (postSize==preSize+1):"Not a valid addition!";
			}
		}
	}
	
	public  void removeOrder(int id, Date date, int table) {
		assert ( table >= 0 && id >= 0) :"Cannot add with null table/id and/or negative !";
		int preSize = database.size();
		if (isWellFormed()) {
			Order p = new Order(id, date,table);
			if (database.containsKey(p)) {
				database.remove(p);
				int postSize=database.size();
				assert (postSize==preSize-1):""+"Not a valid deletion!";
			} else
				infoBox("Order does not exist!", "Remove order error.");
		}
	}
	
	public  void addMeniu(Order order, CompositeProduct cproduct) {
		assert (order!=null && cproduct != null):"Cannot add menu if null order and/or negative products!";
		if (isWellFormed()) {
			if (database.containsKey(order)) {		
					
					HashSet<MenuItem> list = (HashSet<MenuItem>) database.get(order);
					int presize=list.size();
					list.add(cproduct);
					int postsize=list.size();
					assert (postsize==presize+1):"Invalid add Menu!";
					setId(getId() + 1);
					database.put(order, list);
					

				
				}
			} else
				infoBox("Menu does not exist!", "Add error.");
	}
	
	public Order getOrder(int table) {
		
		if(isWellFormed()) {
			Iterator<Order> i = database.keySet().iterator();
			while(i.hasNext()) {
				if(i.next().getTable() == table) {
					return i.next();
				}
			}
		}
		return null;
	}
	
	public HashSet<MenuItem> getMenu(Order o){
		
		if(isWellFormed()) {
			if(database.containsKey(o)) {
				return database.get(o);
			}
		}
		
		return null;
	}
	
	
	
	private  boolean isWellFormed() {
		if (database == null)
			return false;
		return true;
	}
	
	public static  void infoBox(String infoMessage, String titleBar) {
		JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public HashMap<Order, HashSet<MenuItem>> getData(){
		
		return database;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
