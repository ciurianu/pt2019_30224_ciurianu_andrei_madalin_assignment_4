package Restaurant;

public interface IRestaurantProcessing {
	
	
	public void CreateMenuItem();
	public void DeleteMenuItem();
	public void EditMenuItem();
	public void CreateNewOrder();
	public void ComputePrice();
	public void CenerateBill();
   
}
